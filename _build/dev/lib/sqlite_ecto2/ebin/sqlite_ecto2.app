{application,sqlite_ecto2,
             [{description,"SQLite3 adapter for Ecto2"},
              {modules,['Elixir.DBConnection.Query.Sqlite.DbConnection.Query',
                        'Elixir.Enumerable.Sqlite.DbConnection.Stream',
                        'Elixir.Sqlite.DbConnection.App',
                        'Elixir.Sqlite.DbConnection.Error',
                        'Elixir.Sqlite.DbConnection.Protocol',
                        'Elixir.Sqlite.DbConnection.Query',
                        'Elixir.Sqlite.DbConnection.Result',
                        'Elixir.Sqlite.DbConnection.Stream',
                        'Elixir.Sqlite.Ecto2',
                        'Elixir.Sqlite.Ecto2.Connection',
                        'Elixir.String.Chars.Sqlite.DbConnection.Query']},
              {registered,[]},
              {vsn,"2.2.2"},
              {applications,[kernel,stdlib,elixir,db_connection,ecto,logger,
                             sqlitex]},
              {mod,{'Elixir.Sqlite.DbConnection.App',[]}}]}.
